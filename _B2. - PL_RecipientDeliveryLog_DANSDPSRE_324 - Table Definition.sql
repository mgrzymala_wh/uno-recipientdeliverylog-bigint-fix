USE [PresentationLayer]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_DANSDPSRE_324]
GO

CREATE TABLE [dbo].[RecipientDeliveryLog_DANSDPSRE_324](
	[RecipientDeliveryLogId] BIGINT NOT NULL,
	[AdobeDeliveryLogId] [int] NOT NULL,
	[DeliveryId] [int] NOT NULL,
	[CustomerAccountId] [int] NOT NULL,
	[DeliveryLogStatusId] [int] NULL,
	[Address] [varchar](512) NULL,
	[EventDt] [datetime] NULL,
	[ControlGroupFlag] [varchar](1) NULL,
	[InformationSourceId] [int] NOT NULL,
	[SystemCreatedDt] [datetime] NOT NULL,
	[UpdateByStaffId] [int] NOT NULL,
	[SystemUpdatedDt] [datetime] NOT NULL,
	[UVSTierId] [int] NOT NULL,
	[CustomerAccountActivityStatusId] [int] NOT NULL,
	[CustomerAccountActivityTypeId] [int] NOT NULL,
	[PrimaryDeliveryGroup] [varchar](50) NULL,
	[SecondaryDeliveryGroup] [varchar](50) NULL,
	[OfferAmount] [decimal](18, 5) NULL,
	[RequiredBetAmount] [decimal](18, 5) NULL,
	[PlayerClaimlistCode] [varchar](50) NULL,
	[PlusAccountId] [int] NOT NULL
) ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE CLUSTERED COLUMNSTORE INDEX [IX_C_Col_dbo_RecipientDeliveryLog] ON [dbo].[RecipientDeliveryLog_DANSDPSRE_324] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0)
GO


ALTER TABLE [dbo].[RecipientDeliveryLog_DANSDPSRE_324] ADD CONSTRAINT [DF_dbo_RecipientDeliveryLog_UVSTierId2]  DEFAULT ((2)) FOR [UVSTierId]
ALTER TABLE [dbo].[RecipientDeliveryLog_DANSDPSRE_324] ADD CONSTRAINT [DF_dbo_RecipientDeliveryLog_CustomerAccountActivityStatus2]  DEFAULT ((2)) FOR [CustomerAccountActivityStatusId]
ALTER TABLE [dbo].[RecipientDeliveryLog_DANSDPSRE_324] ADD CONSTRAINT [DF_dbo_RecipientDeliveryLog_CustomerAccountActivityType2]  DEFAULT ((2)) FOR [CustomerAccountActivityTypeId]
ALTER TABLE [dbo].[RecipientDeliveryLog_DANSDPSRE_324] ADD CONSTRAINT [DF_RecipientDeliveryLog_PlusAccountId2]  DEFAULT ((2)) FOR [PlusAccountId]
GO

EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'5C503E21-22C6-81FA-620B-F369B8EC38D1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'Address'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Contact Info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'Address'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'989ADC05-3F3F-0588-A635-F475B994915B' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'Address'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential - GDPR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'Address'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityStatusId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityStatusId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityStatusId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityStatusId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityTypeId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityTypeId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityTypeId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'CustomerAccountActivityTypeId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'OfferAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'OfferAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'OfferAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'OfferAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'RequiredBetAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'RequiredBetAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'RequiredBetAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'RequiredBetAmount'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'PlusAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'PlusAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'PlusAccountId'
EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RecipientDeliveryLog_DANSDPSRE_324', @level2type=N'COLUMN',@level2name=N'PlusAccountId'
GO
