USE [PresentationLayer]
GO


DROP TABLE IF EXISTS [RecipientTrackingLog_DANSDPSRE_324]
GO

CREATE TABLE [dbo].[RecipientTrackingLog_DANSDPSRE_324](
	[RecipientTrackingLogId] BIGINT IDENTITY(1,1) NOT NULL,
	[AdobeTrackingLogId] [INT] NOT NULL,
	[RecipientDeliveryLogId] BIGINT NOT NULL,
	[LogDt] [DATETIME] NULL,
	[LogType] [VARCHAR](20) NULL,
	[SourceURL] [VARCHAR](1000) NULL,
	[InformationSourceId] [INT] NOT NULL,
	[SystemCreatedDt] [DATETIME] NOT NULL,
	[UpdateByStaffId] [INT] NOT NULL,
	[SystemUpdatedDt] [DATETIME] NOT NULL,
 CONSTRAINT [PK_dbo_RecipientTrackingLog_BigInt] PRIMARY KEY CLUSTERED 
(
	[RecipientTrackingLogId] ASC,
    [SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ps_daily_date]([SystemUpdatedDt])
) ON [ps_daily_date]([SystemUpdatedDt])
GO


-- this constraint has to be abandoned because you can not create a constraint referencing a view
--ALTER TABLE [dbo].[RecipientTrackingLog_DANSDPSRE_324]  WITH CHECK ADD  CONSTRAINT [FK_dbo_RecipientTrackingLog_RecipientDeliveryLogId_BigInt] FOREIGN KEY([RecipientDeliveryLogId])
--REFERENCES [dbo].[RecipientDeliveryLog] ([RecipientDeliveryLogId])
--GO
--ALTER TABLE [dbo].[RecipientTrackingLog_DANSDPSRE_324] CHECK CONSTRAINT [FK_dbo_RecipientTrackingLog_RecipientDeliveryLogId_BigInt]
--GO