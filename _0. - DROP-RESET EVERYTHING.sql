USE [BusinessSemanticLayer]
GO

--DROP INDEX IF EXISTS [IX_NCL_dbo_RecipientTrackingLog_SystemUpdatedDt] ON [dbo].[RecipientTrackingLog]
--DROP TABLE IF EXISTS [RecipientTrackingLog_DANSDPSRE_324]
--DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_Archive_NEW]
--DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_CurrentToArchive_Staging]
--DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_Current]
--DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_RecycleBin]


--IF EXISTS (SELECT * FROM sys.partition_schemes WHERE [name] = 'ps_daily_date')
--BEGIN
--    DROP PARTITION SCHEME [ps_daily_date];
--END

--IF EXISTS (SELECT * FROM sys.partition_functions WHERE [name] = 'pf_daily_date')
--BEGIN
--    DROP PARTITION FUNCTION [pf_daily_date];
--END