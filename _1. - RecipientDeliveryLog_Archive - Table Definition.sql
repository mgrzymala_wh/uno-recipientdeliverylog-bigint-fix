USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [dbo].[RecipientDeliveryLog_Archive_NEW]

CREATE TABLE [dbo].[RecipientDeliveryLog_Archive_NEW](
	[RecipientDeliveryLogId] BIGINT IDENTITY(1,1) NOT NULL,
	[AdobeDeliveryLogId] [int] NOT NULL,
	[DeliveryId] [int] NOT NULL,
	[CustomerAccountId] [int] NOT NULL,
	[DeliveryLogStatusId] [int] NULL,
	[Address] [varchar](512) NULL,
	[EventDt] [datetime] NULL,
	[ControlGroupFlag] [varchar](10) NULL,
	[InformationSourceId] [int] NOT NULL,
	[SystemCreatedDt] [datetime] NOT NULL,
	[UpdateByStaffId] [int] NOT NULL,
	[SystemUpdatedDt] [datetime] NOT NULL,
	[UVSTierId] [int] NOT NULL,
	[CustomerAccountActivityStatusId] [int] NOT NULL,
	[CustomerAccountActivityTypeId] [int] NOT NULL,
	[PrimaryDeliveryGroup] [varchar](50) NULL,
	[SecondaryDeliveryGroup] [varchar](50) NULL,
	[OfferAmount] [decimal](18, 5) NULL,
	[RequiredBetAmount] [decimal](18, 5) NULL,
	[PlayerClaimlistCode] [varchar](50) NULL,
	[PlusAccountId] [int] NULL
    ) ON [ps_daily_date]([SystemUpdatedDt])
GO

--
CREATE CLUSTERED COLUMNSTORE INDEX [IX_C_Col_dbo_RecipientDeliveryLog_Archive_NEW] ON [dbo].[RecipientDeliveryLog_Archive_NEW] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0, DATA_COMPRESSION = COLUMNSTORE_ARCHIVE)
GO
--
ALTER TABLE [dbo].[RecipientDeliveryLog_Archive_NEW] ADD CONSTRAINT [PK_dbo_RecipientDeliveryLog_Archive_NEW_RecipientDeliveryLogId] PRIMARY KEY NONCLUSTERED 
(
	[RecipientDeliveryLogId] ASC,
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ON [ps_daily_date]([SystemUpdatedDt])
GO
--
CREATE NONCLUSTERED INDEX [IX_NCL_dbo_RecipientDeliveryLog_Archive_NEW_AdobeDeliveryLogId] ON [dbo].[RecipientDeliveryLog_Archive_NEW]
(
	[AdobeDeliveryLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ON [ps_daily_date]([SystemUpdatedDt])
GO
--

DECLARE @SystemUpdatedDtMin DATETIME, @SystemUpdatedDtMax DATETIME, @Sql VARCHAR(1024)

SELECT      @SystemUpdatedDtMin = CONVERT(DATETIME, MIN(sprv.value)),
            @SystemUpdatedDtMax = CONVERT(DATETIME, MAX(sprv.value))
FROM        sys.partition_functions AS spf
INNER JOIN  sys.partition_range_values sprv ON sprv.function_id=spf.function_id
WHERE       (spf.name = N'pf_daily_date')

--SELECT @Sql = 'ALTER TABLE [dbo].[RecipientDeliveryLog_Archive_NEW] WITH CHECK ADD CONSTRAINT [DF_dbo_RecipientDeliveryLog_Archive_NEW_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)+''')'
SELECT @Sql = 'ALTER TABLE [dbo].[RecipientDeliveryLog_Archive_NEW] WITH CHECK ADD CONSTRAINT [DF_dbo_RecipientDeliveryLog_Archive_NEW_SystemUpdatedDt] CHECK (SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)+''')'
PRINT 'Processing table: [RecipientDeliveryLog_Archive_NEW], adjusting Lowest/Highest-Value-Constraints to: < '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)
--PRINT(@Sql)
EXEC(@Sql)
GO

--TRUNCATE TABLE [dbo].[RecipientDeliveryLog_Archive_NEW]
