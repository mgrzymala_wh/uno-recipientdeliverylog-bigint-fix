USE [BusinessSemanticLayer]
GO


DROP INDEX IF EXISTS [IX_NCL_dbo_RecipientTrackingLog_SystemUpdatedDt] ON [dbo].[RecipientTrackingLog]
GO

CREATE NONCLUSTERED INDEX [IX_NCL_dbo_RecipientTrackingLog_SystemUpdatedDt] ON [dbo].[RecipientTrackingLog]
(
	[SystemUpdatedDt] ASC
)
WHERE [SystemUpdatedDt] > '2020-03-01 00:00:00'
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ps_daily_date]([SystemUpdatedDt])
GO
